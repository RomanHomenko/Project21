//
//  ViewController.swift
//  Project21
//
//  Created by Роман Хоменко on 24.06.2022.
//

import UserNotifications
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Register", style: .done, target: self, action: #selector(registerLocal))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Schedule", style: .done, target: self, action: #selector(scheduleLocal))
    }
}

// MARK: - Helper @objc methods
extension ViewController {
    @objc func registerLocal() {
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
            if granted {
                print("Yay")
            } else {
                print("FUCK")
            }
        }
    }

    @objc func scheduleLocal() {
        registerCategories()
        
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
        // What is a content
        let content = UNMutableNotificationContent()
        content.title = "Late wake up call"
        content.body = "WAKE THE FUCK UP SAMURAI!!! THERE IS A CITY TO BURN!!!"
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "Kiany Rivz"]
        content.sound = .default
        
        // When show the content
        var dateComponents = DateComponents()
        dateComponents.hour = 10
        dateComponents.minute = 30
//        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents,
//                                                    repeats: true)
        // after button trigger, receive this after 24 hours again
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 86400, repeats: false)
        
        // Make a request
        let request = UNNotificationRequest(identifier: UUID().uuidString,
                                            content: content,
                                            trigger: trigger)
        center.add(request)
    }
}

// UNUserNotificationCenterDelegate
extension ViewController: UNUserNotificationCenterDelegate {
    func registerCategories() {
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        let show = UNNotificationAction(identifier: "show",
                                        title: "Tell me more",
                                        options: .foreground)
        let reminder = UNNotificationAction(identifier: "reminder",
                                            title: "Remind me later",
                                            options: .foreground)
        let category = UNNotificationCategory(identifier: "alarm",
                                              actions: [show, reminder],
                                              intentIdentifiers: [])
        
        center.setNotificationCategories([category])
    }
}

// Implement UNUserNotifications methods
extension ViewController {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        if let customData = userInfo["customData"] as? String {
            print("Custom data received: \(customData)")
            
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                // the user swipe it to unlock
//                print("default identifier")
                createAlert()
            case "show":
                createAlert(type: "show")
//                print("show more information...")
            case "reminder":
                scheduleLocal()
            default:
                break
            }
        }
        
        completionHandler()
    }
}

extension ViewController {
    func createAlert(type: String? = nil) {
        var alert: UIAlertController?
        
        if let currentType = type {
            alert = UIAlertController(title: "You tapped \(currentType)",
                                          message: "Try again!",
                                          preferredStyle: .alert)
        } else {
            alert = UIAlertController(title: "You tapped default",
                                          message: "Try again!",
                                          preferredStyle: .alert)
        }
        alert!.addAction(UIAlertAction(title: "Ok",
                                      style: .default))
        present(alert!, animated: true)
    }
}
